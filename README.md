# aster-cloud

#### 介绍
基于SpringBoot2.4.2、SpringCloud2020.0.x、SpringCloud Alibaba2021.1搭建的微服务系统，
技术选型Nacos、gateway、openFeign、loadbalancer、sentinel、seata、mybatis-plus、vue等，
支持前后端代码快速生成。

#### 软件架构
|  软件架构   | 技术选型  |
|  ----  | ----  |
|微服务框架：| Spring Cloud 2020.0.1 & Spring Cloud Alibaba 2021.1 |
|开发框架：| Spring Boot 2.4.2 |
|服务注册与发现：| Nacos 1.4.1 |
|服务配置：| Nacos 1.4.1 |
|服务熔断降级/流量控制：| Sentinel 1.8.0 |
|服务网关：| Spring Cloud Gateway |
|服务调用：| Spring Cloud OpenFeign |
|负载均衡：| Spring Cloud Loadbalancer |
|分布式事务：| Seata 1.3.0 |
|服务总线：| Nacos 1.4.1 |
|链路追踪：| Spring Cloud Sleuth + Zipkin 替代：SkyWalking |
|日志收集：| ELK(logstash) / EFK(fluentd)  elasticsearch + ? + kibana |
|消息队列：| 中小型RabbitMQ  /  大型（kafka / rocketMq）|
|服务监控：| Prometheus + Grafana / Spring Cloud actuator + admin |
|自动化部署：| Docker / k8s |
|安全认证授权：| Spring Security Oauth2 / Jwt |
|分布式缓存：| Redis |
|全文检索：| ES（ElatisticSearch）|
|对象存储：| OSS、MinIO |
|任务调度：| XXL-Job 、Quartz |
|持久化框架：| Mybatis-Plus |
|分库分表：| Sharding-jdbc |
|数据库连接池：| Druid |
|接口文档：| knife4j / Swagger |
|工作流：| Activity |
|JS框架：| Vue 、Uni-app(H5) 、Nodejs |
|组件库：| ElementUI(PC) / Vant(H5) |
|打包工具：| WebPack(PC) / HbuilderX |
|开发工具：| IntelliJ IDEA 、VS Code、HbuilderX |




#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
